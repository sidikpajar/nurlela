<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    <a href="seminar-add.php" class='btn btn-primary'><i class="fas fa-plus"></i> Add Seminar</a>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="seminar.php">Seminar</a></li>
                    <li class="breadcrumb-item active">Seminar</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" style="font-size:14px">
        <div class="container">

        <div class="">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                    <table id="dataTables" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name Event</th>
                                <th>Date</th>
                                <th>Quota</th>
                                <th>Pembayaran</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $show_seminar = mysqli_query($connect,"SELECT * FROM seminar");
                            while($row = mysqli_fetch_array($show_seminar)) {
                        ?>
                        <tr>
                            <td>
                                <?php 
                                    echo $row['name']; 
                                    echo "<br/><i style='font-size: 13px'> Posted on: ".$row['date_post']."</i>";
                                ?>
                            </td>
                            <td><?php echo $row['date_event'] ?></td>
                            <td><?php echo $row['quota'] ?></td>
                            <td>
                                <?php 
                                    if($row['pay_status'] == 1){
                                        echo "Paid - Rp", number_format($row['value']);
                                    } else {
                                        echo "Free";
                                    }
                                ?>
                            </td>
                            <td>
                            <?php 
                              // cek status, jika status 1 maka akan muncul Aktif, jika 0 maka muncul Tidak aktif
                              if ($row['seminar_status']==1){
                                echo "<a href='seminar-update-to-off.php?id_seminar=".$row['id']." ' class='btn btn-success  btn-xs' '> Active </a>";
                              } else echo "<a href='seminar-update-to-on.php?id_seminar=".$row['id']." ' class='btn btn-danger  btn-xs' '> In Active </a>";
                              ?>
                            </td>
                            </td>
                            <?php
                                echo "
                                    <td>
                                    <a class=' btn btn-xs btn-primary' href='seminar-detail.php?id_seminar=".$row['id']."'>Detail</a>
                                    <a class='btn btn-xs btn-warning' href='seminar-edit.php?id_seminar=".$row['id']."'>Edit</a>
                                    </td>
                                ";
                            ?>
                        </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            <!-- /.box-body -->
            </div>
        <!-- /.box -->
            </div>
        </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php include("component/footer.php")?>