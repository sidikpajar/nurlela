t<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                  <a href="javascript:HistoryFunction()" class='btn btn-primary'><i class="fas fa-arrow-left"></i> Back</a> Payment
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="seminar.php">Seminar</a></li>
                    <li class="breadcrumb-item active">Seminar</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" style="font-size:14px">
        <div class="container">

            <div class="col-12 col-sm-12 col-lg-12">
              <div class="card card-primary card-outline ">
                <div class="card-header p-0 border-bottom-0">
                  <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Seminar Informations</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                  <div class="tab-content" id="custom-tabs-three-tabContent">

                    <div class="tab-pane fade active show" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                      <a href="javascript:RefreshFunction()" class='btn btn-xs btn-primary' style='margin-bottom: 20px'><i class='fas fa-sync'></i> Refresh</a>

                        <div class="row">
                          <div class="col-4 col-sm-12 col-lg-4">
                            <table class="table table-bordered">
                              <tbody>
                                <tr>
                                  <th colspan="2">Details</th>
                                </tr>
                                  <?php
                                    $id_participant = $_GET['id_participant'];
                                    $sql1="SELECT * FROM seminar_participant WHERE id='$id_participant' ";
                                    $query = mysqli_query( $connect, $sql1 );
                                    while($row = mysqli_fetch_array( $query )) {
                                  ?>
                                <tr>
                                  <td>Payment Value</td>
                                  <td>
                                    <?php echo $row['payment_value']; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Atas Nama</td>
                                  <td>
                                    <?php echo $row['on_behalf']; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Tanggal</td>
                                  <td>
                                    <?php echo $row['create_at']; ?>
                                  </td>
                                </tr>
                      
                              <?php } ?>
                              </tbody>
                            </table>
                          </div>
                            <div class="col-8 col-sm-12 col-lg-8">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <th>Bukti Transfer</th>
                                      <th style="text-align: right">
                                      <?php 
                                        $id_participant = $_GET['id_participant'];
                                        echo "<a href='seminar-detail-payment-confirmation.php?id_participant=".$id_participant." ' class='btn btn-md btn-success' '> Konfirmasi Pembayaran </a>";
                                      ?>
                                      </th>
                                    </tr>
                                      <?php
                                        $id_participant = $_GET['id_participant'];
                                        $sql1="SELECT * FROM seminar_participant WHERE id='$id_participant' ";
                                        $query = mysqli_query( $connect, $sql1 );
                                        while($row = mysqli_fetch_array( $query )) {
                                            $url = $row['foto']; 
                                            echo "<tr>";
                                            echo "<td colspan='2'>";
                                            echo "<img src='../files-gambar/" . $url . "' class='d-block w-100' alt='Bukti Pembayaran'>";
                                            echo "</td>";
                                            echo "</tr>";
                                        }
                                      ?>
                                  
                                  </tbody>
                                </table>
                            </div>
                          </div>
                    </div>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            </div>


        </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
      function RefreshFunction() {
        location.reload();
      }
      function HistoryFunction() {
        window.history.back();
      }
    </script>


<?php include("component/footer.php")?>