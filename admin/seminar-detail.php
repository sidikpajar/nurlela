<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                  <a href="seminar.php" class='btn btn-primary'><i class="fas fa-arrow-left"></i> Back</a> Data Seminar
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="seminar.php">Seminar</a></li>
                    <li class="breadcrumb-item active">Seminar</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" style="font-size:14px">
        <div class="container">

            <div class="col-12 col-sm-12 col-lg-12">
              <div class="card card-primary card-outline ">
                <div class="card-header p-0 border-bottom-0">
                  <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Seminar Informations</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Participants</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="custom-tabs-three-messages-tab" data-toggle="pill" href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages" aria-selected="false">Seminar Documents</a>
                    </li>

                  </ul>
                </div>
                <div class="card-body">
                  <div class="tab-content" id="custom-tabs-three-tabContent">

                    <div class="tab-pane fade active show" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">

                      <?php
                        $id_seminar = $_GET['id_seminar'];
                        echo "<a href='seminar-edit.php?id_seminar=".$id_seminar." ' class='btn btn-xs btn-warning' style='margin-bottom: 20px'><i class='fas fa-edit'></i> Edit Seminar</a>";
                      ?>
                      <a href="javascript:RefreshFunction()" class='btn btn-xs btn-primary' style='margin-bottom: 20px'><i class='fas fa-sync'></i> Refresh</a>

                        <div class="row">
                          <div class="col-4 col-sm-12 col-lg-4">
                            <table class="table table-bordered">
                              <tbody>
                                <tr>
                                  <th colspan="2">Details</th>
                                </tr>
                                  <?php
                                    $seminar_id = $_GET['id_seminar'];
                                    $sql1="SELECT * FROM seminar WHERE id='$seminar_id' ";
                                    $query = mysqli_query( $connect, $sql1 );
                                    while($row = mysqli_fetch_array( $query )) {
                                  ?>
                                <tr>
                                  <td>Title</td>
                                  <td>
                                    <?php echo $row['name']; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Posted On</td>
                                  <td>
                                    <?php echo $row['date_post']; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Date Event</td>
                                  <td>
                                    <?php echo $row['date_event']; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Quota</td>
                                  <td>
                                    <?php echo $row['quota']; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Pay Status</td>
                                  <td>
                                    <?php 
                                          if($row['pay_status'] == 1){
                                              echo "Paid - Rp", number_format($row['value']);
                                          } else {
                                              echo "Free";
                                          }
                                      ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Event Status</td>
                                  <td>
                                    <?php 
                                      if($row['seminar_status'] == 1){
                                          echo "Active";
                                      } else {
                                          echo "In Active";
                                      }
                                    ?>
                                  </td>
                                </tr>
                              <?php } ?>
                              </tbody>
                            </table>
                          </div>
                            <div class="col-8 col-sm-12 col-lg-8">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <th>Descriptions</th>
                                    </tr>
                                      <?php
                                        $seminar_id = $_GET['id_seminar'];
                                        $sql1="SELECT * FROM seminar WHERE id='$seminar_id' ";
                                        $query = mysqli_query( $connect, $sql1 );
                                        while($row = mysqli_fetch_array( $query )) {
                                      ?>
                                    <tr>
                                      <td>
                                        <?php echo $row['description']; ?>
                                      </td>
                                    </tr>
                                  <?php } ?>
                                  </tbody>
                                </table>
                            </div>
                          </div>
                    </div>

                    <!-- Tab Participant -->
                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel"     aria-labelledby="custom-tabs-three-profile-tab">
                      <table id="dataTablesParticipants" class="table table-striped table-bordered" style="width:100%">
                          <thead style="text-align: center" >
                              <tr>
                                  <th>Name</th>
                                  <th>Payment</th>
                                  <th>Certificate</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php
                                $seminar_id = $_GET['id_seminar'];
                                $show_participant = mysqli_query($connect,
                                "SELECT *, seminar_participant.id as id_participant FROM seminar_participant 
                                  INNER JOIN users
                                  ON users.id = seminar_participant.id_user
                                    WHERE id_seminar='$seminar_id' 
                                ");
                                    while($row = mysqli_fetch_array($show_participant)) {
                              ?>
                                <tr>
                                    <td width="20%"><?php echo $row['name'];?></td>
                                    <td width="30%" style='text-align: center'>
                                      <?php 
                                        if($row['payment_status'] == 0){
                                          echo "<a class=' btn btn-xs btn-warning' href='seminar-detail-payment.php?id_participant=".$row['id_participant']."'>Pending</a>";
                                        } elseif($row['payment_status'] == 1) {
                                          echo "<a class=' btn btn-xs btn-info' href='seminar-detail-payment.php?id_participant=".$row['id_participant']."'>On Checking</a>";
                                        } elseif($row['payment_status'] == 2) {
                                          echo "<a class=' btn btn-xs btn-success' href='seminar-detail-payment.php?id_participant=".$row['id_participant']."'>Success</a>";
                                        } elseif($row['payment_status'] == 3) {
                                          echo "<a class=' btn btn-xs btn-success' href='seminar-detail-payment.php?id_participant=".$row['id_participant']."'>Free Seminar</a>";
                                        } else {
                                          echo "<a class=' btn btn-xs btn-danger' href='seminar-detail-payment.php?id_participant=".$row['id_participant']."'>Reject</a>";
                                        }
                                      ?>
                                    </td>
                                    <td width="50%">
                                      <form role="form" method="POST" action="seminar-detail.php?id_seminar=<?php echo $seminar_id; ?>" enctype="multipart/form-data">
                                        <div class="row">
                                          <div class="col-sm-8">
                                            <?php 
                                                if($row['value'] === ''){
                                                  echo "<a class='btn btn-xs btn-danger' style='color:white; font-weight:500; text-align: left;' href='#'>Non Data</a>";
                                                } else {
                                                  echo "<a class='btn btn-xs btn-primary' style='color:white; font-weight:500; text-align: left;' href='files-certificate/".$row['value']."' download>Downloads</a>";
                                                }
                                            ?>
                                            <input type="file" id="value" name="value">
                                            <input type="hidden" name="id_participant" value=<?php echo $row['id_participant']; ?>>
                                          </div>
                                          <div class="col-sm-3">
                                            <button type="submit" name="submit" class="btn btn-xs btn-primary">Save Certificate</button>
                                          </div>
                                        </div>
                                      </form>
                                      <?php  include("seminar-detail-upload-certificate-proses.php");?>
                                    </td>
                                </tr>
                              <?php
                              }
                              ?>
                          </tbody>
                      </table>
                    </div>

                    <!-- Tab Document -->
                    <div class="tab-pane fade" id="custom-tabs-three-messages" role="tabpanel" aria-labelledby="custom-tabs-three-messages-tab">
                      <?php
                        $id_seminar = $_GET['id_seminar'];
                        echo "<a href='seminar-document-add.php?id_seminar=".$id_seminar." ' class='btn btn-xs btn-info' style='margin-bottom: 20px'><i class='fas fa-plus'></i> Add Document</a>";
                      ?>
                      <a href="javascript:RefreshFunction()" class='btn btn-xs btn-primary' style='margin-bottom: 20px'><i class='fas fa-sync'></i> Refresh</a>
                        <table id="dataTables" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Document Id</th>
                                    <th>Document Name</th>
                                    <th>Download File</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $show_document = mysqli_query($connect,"SELECT * FROM seminar_document");
                                while($row = mysqli_fetch_array($show_document)) {
                            ?>
                            <tr>
                                <td>
                                    <?php 
                                        echo $row['id']; 
                                    ?>
                                </td>
                                <td><?php echo $row['name'] ?></td>
                                <td>
                                <?php
                                  echo "<a style='color:blue; font-weight:500;' href='files-document/".$row['value']."' download> ".$row['value']." </a>";
                                ?>
                
                                </td>
                                <?php
                                    echo "
                                        <td>
                                          <a class=' btn btn-xs btn-danger' href='seminar-document-delete.php?id_document=".$row['id']."'>Delete</a>
                                        </td>
                                    ";
                                ?>
                            </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>


                    </div>

                  </div>
                </div>
                <!-- /.card -->
              </div>
            </div>


        </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
      function RefreshFunction() {
        location.reload();
      }
    </script>
<?php include("component/footer.php")?>