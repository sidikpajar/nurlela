<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    <?php
                      $id_seminar = $_GET['id_seminar'];
                      echo "<a href='seminar-detail.php?id_seminar=".$id_seminar." ' class='btn btn-warning'><i class='fas fa-arrow-left'></i> Back</a>";
                    ?>
                    Add Document
                </h1>
               
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="seminar.php">Seminar</a></li>
                    <li class="breadcrumb-item active">Document</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
      <section class="content" style="font-size:14px">
          <div class="container">
            <div class="box">
              <div class="box-body">



              <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Please fill this from</h3>
              </div>
              <!-- /.card-header -->

              <!-- form start -->
              <form role="form" method="POST" action="seminar-document-add.php" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Document Name</label>
                    <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Name of your document" required>
                  </div>
                  <div class="form-group">
                        <label for="exampleInputEmail1">Upload File</label>
                        <input type="file" class="form-control" id="value" name="value" required>
                  </div>

                  <?php
                        $id_seminar = $_GET['id_seminar'];
                        echo "<input type='hidden' value='".$id_seminar."' name='id_seminar'>";
                  ?>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>

                <?php
                    if(isset($_POST["submit"])){
                        $check = $_FILES["value"]["tmp_name"];
                        if($check !== false){
                            $id             = rand(1111,9999);
                            $lokasi_file     = $_FILES['value']['tmp_name'];
                            $nama_file       = $_FILES['value']['name'];
                            $folder         = "files-document/$id+$nama_file";
                            $name           = $_POST['name'];
                            $id_seminar     = $_POST['id_seminar'];

                            if (move_uploaded_file($lokasi_file,"$folder")){
                              echo "Nama File : <b>$nama_file</b> sukses di upload";

                              
                              $sql2 = "INSERT INTO seminar_document (id, id_seminar, name, value) VALUES ('$id','$id_seminar','$name','$id+$nama_file')";
                              if ($connect-> query($sql2) === TRUE) {
                                  echo "
                                  <script type= 'text/javascript'>
                                      window.location = 'seminar-detail.php?id_seminar=".$id_seminar."';
                                  </script>";
                                  } else {
                                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                      }

                            }
                            else{
                              echo "
                              <script type= 'text/javascript'>
                                  var r = confirm('Try again?');
                                  if (r == true) {
                                    window.location = 'seminar-document-add.php?id_seminar=".$id_seminar."';
                                  } else {
                                    window.location = 'seminar.php';
                                  }
                              </script>";
                            }

                        }else{
                            echo "Please select an pdf file to upload.";
                        }
                    }
                    ?>

              </form>
            </div>


              </div>
            </div>
          </div>
      </section>
</div>
    <!-- /.content-wrapper -->
<?php include("component/footer.php")?>