<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    <?php
                      echo "<a href='seminar.php?' class='btn btn-warning'><i class='fas fa-arrow-left'></i> Back</a>";
                    ?>
                    Add Seminar
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="seminar.php">Seminar</a></li>
                    <li class="breadcrumb-item active">Seminar</li>
                </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
      <section class="content" style="font-size:14px">
          <div class="container">
              <div class="box">
                  <div class="box-body">
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">Add Form</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="seminar-add.php" enctype="multipart/form-data">
                      <div class="card-body">

                        <div class="form-group">
                          <label>Seminar Name</label>
                          <input type="text" class="form-control" id="name" name="name" required
                          placeholder="Name of Seminar, Event, or Activity">
                        </div>

                        <div class="form-group">
                          <label>Date Event</label>
                          <input type="text" class="form-control" id="date_event" name="date_event" placeholder="Date of event" required>
                        </div>

                        <div class="form-group">
                          <label>Quota</label>
                          <input type="number" class="form-control" id="quota" name="quota" placeholder="Quota of Participants" required>
                        </div>

                        <div class="form-group">
                          <label>Pay Status</label>

                          <select id="pay_select" name="pay_status" class="form-control">
                            <option id="berbayar" value="1">Paid</option>
                            <option id="gratis" value="0">Free</option>
                          </select>

                        </div>

                    

                        <div style="display:none;" id="cost_form" class="form-group">
                          <label>Cost</label>
                          <input type="number" name="value" class="form-control" placeholder="How Much?">
                        </div>

                        <div class="form-group">
                          <label>Description</label>
                          <textarea class="fr-view" id="example" name="description"></textarea>
                        </div>

                      </div>
                      <!-- /.card-body -->

                      <div class="card-footer">
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>
                    

                    <?php
                    if(isset($_POST["submit"])) {
                      $id_seminar           = rand(1111,9999);
                      $name                 = $_POST['name'];
                      $date_event           = $_POST['date_event'];
                      $quota                = $_POST['quota'];
                      $pay_status           = $_POST['pay_status'];
                      $seminar_status       = 1;
                      $value                = $_POST['value'];
                      $description          = $_POST['description'];
                      $date_post            = (new DateTime('now'))->format('Y-m-d H:i:s');

                      $sql = "INSERT INTO seminar (id,  name, date_post, date_event, quota, description, pay_status, seminar_status, value)
                              VALUES ('$id_seminar','$name','$date_post','$date_event','$quota', '$description', '$pay_status', '$seminar_status', '$value')
                              ";

                      if ($connect-> query($sql) === TRUE ) {
                      echo "
                      <script type= 'text/javascript'>
                          alert('Seminar ".$name." has been add');
                          window.location = 'seminar.php ';
                      </script>";

                      } else {
                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                      }
                      $connect->close();
                      }

                  
                  ?>
                  </div>

                  </div>
              </div>
          </div>
      </section>

    </div>
<?php include("component/footer.php")?>