<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8" />
    <meta name="author" content="Script Tutorials" />
    <title>Presensi MyIndoversary 2019</title>
    <link href="styles.css" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <!-- Main css -->
    <link rel="stylesheet" href="css/style.css">  
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

</head>
<body>
<div id='stars'></div>
<div id='stars2'></div>
<div id='stars3'></div>
<?php
include("db_connect.php");
?>

<div class="main">
<!-- Sing in  Form -->
<section class="sign-in" style="margin-top: -130px;">
    <div class="container">
            <div class="signin-form">
                
                <img style="padding-bottom:30px; margin-top:-50px;" src="stei.png"/>
                <form style="margin-bottom: -50px" role="form" method="POST" action="register.php" enctype="multipart/form-data" class="register-form" id="login-form">
                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px; " type="text" name="user_name" id="user_name" placeholder="Input Your NIM" required/>
                    </div>

                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px; " type="password" name="user_password" id="user_password" placeholder="Input Your Password" required/>
                    </div>

                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px; " type="text" name="name" id="name" placeholder="Input Full Name" required/>
                    </div>

                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px;" type="email" name="email" id="email" placeholder="Input Mail" required/>
                    </div>

                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px;" type="text" name="institution" id="institution" placeholder="Input Institution" required/>
                    </div>

                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px;" type="text" name="study_program" id="study_program" placeholder="Input Study Program" required/>
                    </div>

                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px; " type="text" name="address" id="address" placeholder="Input Study Address" required/>
                    </div>

                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px; " type="text" name="phone" id="phone" placeholder="Input Phone" required/>
                    </div>

                    <div class="form-group form-button">
                        <button style="font-size:15px;" type="submit" name="submit" id="signin" class="form-submit">Register</button>
                        
                    </div>
                    <div class="form-group form-button">
                         <a href="index.php" style="text-decoration: none; font-size:15px;">I already member</a>
                    <div>
                    <?php 
                    if(isset($_POST['submit']))
                    {   
                        $id             = rand(1,1000);
                        $name           = strtoupper($_POST['name']);
                        $user_name      = $_POST['user_name'];
                        $user_password  = $_POST['user_password'];
                        $email          = $_POST['email'];
                        $institution    = $_POST['institution'];
                        $study_program  = $_POST['study_program'];
                        $address        = $_POST['address'];
                        $phone          = $_POST['phone'];
                        $user_level     = 'peserta';
                        
                        $sql = "INSERT INTO users (
                            id, 
                            user_name, 
                            user_level, 
                            name,
                            user_password,
                            email,
                            institution,
                            study_program,
                            address,
                            phone) VALUES (
                                '$id',
                                '$user_name',
                                '$user_level',
                                '$name',
                                '$user_password',
                                '$email',
                                '$institution',
                                '$study_program',
                                '$address',
                                '$phone')"; 
                            
                        if ($connect-> query($sql) === TRUE ) {
                            echo "
                            <script type= 'text/javascript'>
                                alert('You have successfully register');
                                window.location = 'index.php ';
                            </script>";
      
                            } else {
                            echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                            }
                            $connect->close();
                            
                    }
                    ?>
                </form>
            </div>
    </div>
</section>

</div>

<!-- JS -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>