x
<?php include("component/header.php")?>
    <?php include("component/navbar.php")?>
        <?php include("component/sidebar.php")?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">
                  <a href="pembayaran.php" class='btn btn-primary'><i class="fas fa-arrow-left"></i> Back</a> Data Seminar - Payment
                </h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="pambayaran.php">Payment</a></li>
                                    <li class="breadcrumb-item active">Payment</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content" style="font-size:14px">
                    <div class="container">

                        <div class="col-12 col-sm-12 col-lg-12">
                            <div class="card card-primary card-outline ">
                                <div class="card-header p-0 border-bottom-0">
                                    <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="custom-tabs-three-messages-tab" data-toggle="pill" href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages" aria-selected="false">Payment Information</a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link " id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Seminar Informations</a>
                                        </li>

                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content" id="custom-tabs-three-tabContent">

                                        <div class="tab-pane fade" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-lg-12" style="text-align: right">
                                                    <?php 
                                                      $id_seminar = $_GET['id_seminar'];
                                                      // echo "<a href='seminar-register.php?id_seminar=".$id_seminar."' class='btn btn-md btn-primary' style='margin-bottom: 20px;'><i class='fas fa-plus'></i> Daftar Sekarang </a>";
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 col-sm-12 col-lg-4">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <th colspan="2">Details</th>
                                                            </tr>
                                                            <?php
                                                              $id_seminar = $_GET['id_seminar'];
                                                              $sql1="SELECT * FROM seminar WHERE id='$id_seminar' ";
                                                              $query = mysqli_query( $connect, $sql1 );
                                                              while($row = mysqli_fetch_array( $query )) {
                                                            ?>
                                                                <tr>
                                                                    <td>Title</td>
                                                                    <td>
                                                                        <?php echo $row['name']; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Posted On</td>
                                                                    <td>
                                                                        <?php echo $row['date_post']; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Date Event</td>
                                                                    <td>
                                                                        <?php echo $row['date_event']; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Quota</td>
                                                                    <td>
                                                                        <?php echo $row['quota']; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Pay Status</td>
                                                                    <td>
                                                                        <?php 
                                                                            if($row['pay_status'] == 1){
                                                                                echo "Paid - Rp", number_format($row['value']);
                                                                            } else {
                                                                                echo "Free";
                                                                            }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Event Status</td>
                                                                    <td>
                                                                        <?php 
                                                                          if($row['seminar_status'] == 1){
                                                                              echo "Active";
                                                                          } else {
                                                                              echo "In Active";
                                                                          }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-8 col-sm-12 col-lg-8">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <th>Descriptions</th>
                                                            </tr>
                                                            <?php
                                                              $id_seminar = $_GET['id_seminar'];
                                                              $sql1="SELECT * FROM seminar WHERE id='$id_seminar' ";
                                                              $query = mysqli_query( $connect, $sql1 );
                                                              while($row = mysqli_fetch_array( $query )) {
                                                            ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $row['description']; ?>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Tab Document -->
                                        <div class="tab-pane fade active show" id="custom-tabs-three-messages" role="tabpanel" aria-labelledby="custom-tabs-three-messages-tab">
                                        <div class="row">
                                        <div class="col-6 col-sm-12 col-lg-6">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <th colspan="2">Details</th>
                                                            </tr>
                                                            <?php
                                                              $id_seminar = $_GET['id_seminar'];
                                                              $sql1="SELECT * FROM seminar WHERE id='$id_seminar' ";
                                                              $query = mysqli_query( $connect, $sql1 );
                                                              while($row = mysqli_fetch_array( $query )) {
                                                            ?>
                                                                <tr>
                                                                    <td>Title</td>
                                                                    <td>
                                                                        <?php echo $row['name']; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Payment Status</td>
                                                                    <td>
                                                                        <?php 
                                                                        $id_user           = $_SESSION['userid'];
                                                                        $sql2="SELECT * FROM seminar_participant WHERE id_seminar='$id_seminar' and id_user='$id_user' ";
                                                                        $query2 = mysqli_query( $connect, $sql2 );
                                                                        $id_seminar_participant = '';
                                                                        while($row2 = mysqli_fetch_array( $query2 )) {
                                                                            $value = $row2['payment_status']; 
                                                                            $id_seminar_participant = $row2['id']; 
                                                                            if ($value == 0){
                                                                            echo "Pending";
                                                                            } else if ($value == 1 ){
                                                                            echo "On Checking";
                                                                            } else if ($value == 2 ){
                                                                            echo "Success";
                                                                            } else if ($value == 3 ){
                                                                            echo "Free Seminar";
                                                                            } else {
                                                                            echo "Rejected";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                        </tbody>
                                                    </table>
                                            
                                        </div>
                                        <div class="col-6 col-sm-12 col-lg-6">
                                        <?php 
                                            $id_seminar = $_GET['id_seminar'];
                                            $id_user      = $_SESSION['userid'];
                                            
                                            echo " <a href='pembayaran-detail-upload.php?id_seminar=".$id_seminar."&id_user=".$id_user."&id_seminar_participant=".$id_seminar_participant." ' class='btn btn-xs btn-primary'>Upload Bukti Pembayaran</a> ";
                                            
                                            $sql11="SELECT * FROM seminar_participant WHERE id_seminar='$id_seminar' and id_user='$id_user' ";
                                            $query21 = mysqli_query( $connect, $sql11 );
                                            while($row22 = mysqli_fetch_array( $query21 )) {
                                                $url = $row22['foto']; 
                                                echo "<img src='../files-gambar/" . $url . "' class='d-block w-100' alt='Bukti Pembayaran'>";
                                            
                                            }
                                        ?>
                                    
                                        </div>
                                        </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>

                    </div>
                    <!--/. container-fluid -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <script>
                function RefreshFunction() {
                    location.reload();
                }
            </script>
            <?php include("component/footer.php")?>