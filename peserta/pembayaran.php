<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    </i>List Pembayaran Seminar</a>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="pembayaran.php">Payment</a></li>
                    <li class="breadcrumb-item active">Payment</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Query untuk nampilin list seminar yg udh dibayar

SELECT SP.payment_status AS PAYMENTSTATUS, SP.value AS PHOTO, S.name AS NAMASEMINAR, S.id

	FROM seminar_participant SP

    INNER JOIN seminar S

    	ON SP.id_seminar = S.id

WHERE id_user = 7445 -->

    <!-- Main content -->
    <section class="content" style="font-size:14px">
        <div class="container">

        <div class="">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                    <table id="dataTables" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name Event</th>
                                <th>Biaya</th>
                                <th>Status Pembayaran</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $userid = $_SESSION['userid'];
                        $show_seminar = mysqli_query($connect,"SELECT * FROM seminar_participant where id_user = $userid");
                            while($row = mysqli_fetch_array($show_seminar)) {
                                $id_seminar_participant = $row['id'];
                                $id_seminar = $row['id_seminar'];
                                $paymentStatus = $row['payment_status'];
                        ?>
                        <tr>
                            <?php 
                                $show_seminar_detail = mysqli_query($connect,"SELECT * FROM seminar where id = $id_seminar");
                                while($row2 = mysqli_fetch_array($show_seminar_detail)) {
                                    echo "<td>".$row2['name']."</td>";
                                    echo "<td>".$row2['value']."</td>";
                                }
                            ?>
                            <td>
                                <?php 
                                    if($paymentStatus == 0){
                                        echo "<a href='pembayaran-detail.php?id_seminar=".$id_seminar."' class='btn btn-info  btn-xs'>PENDING - Segera Lakukan Pembayaran</a>";
                                    } else if ($paymentStatus == 1) {
                                        echo "<a href='pembayaran-detail.php?id_seminar=".$id_seminar."' class='btn btn-primary  btn-xs'>ON-CHECKING - Menunggu Konfirmasi Pembayaran oleh Panitia</a>";
                                    } else if ($paymentStatus == 2) {
                                        echo "<a href='pembayaran-detail-success.php?id_seminar=".$id_seminar."' class='btn btn-success  btn-xs'>SUCCESS - Pembayaran Anda telah diterima, Silakan Mengikuti acara sesuai dengan waktu yang ditentukan</a>";
                                    }  else if ($paymentStatus == 3) {
                                        echo "<a href='pembayaran-detail-success.php?id_seminar=".$id_seminar."' class='btn btn-primary  btn-xs'>FREE-SEMINAR - Seminar gratis, Silakan menghadiri acara</a>";
                                    } else {
                                        echo "REJECTED";
                                    }
                                ?>
                            </td>
                        </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            <!-- /.box-body -->
            </div>
        <!-- /.box -->
            </div>
        </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php include("component/footer.php")?>