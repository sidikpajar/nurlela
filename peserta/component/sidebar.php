<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 sidebar-light-primary">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <span class="brand-text font-weight-light">Event Manager</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['name'] ?></a>
        </div>
      </div>
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Home</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="seminar.php" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>Seminar</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="pembayaran.php" class="nav-link">
              <i class="nav-icon fas fa-money-bill-wave-alt"></i>
              <p>Payment</p>
            </a>
          </li>

          <li class="nav-header">Settings</li>
          <li class="nav-item">
            <a href="change-password.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>Change Password</p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="logout.php" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p>Log out</p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>Pembayaran</p>
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>