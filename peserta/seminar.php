<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    </i>List Seminar</a>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="seminar.php">Seminar</a></li>
                    <li class="breadcrumb-item active">Seminar</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Query untuk nampilin list seminar yg udh dibayar

SELECT SP.payment_status AS PAYMENTSTATUS, SP.value AS PHOTO, S.name AS NAMASEMINAR, S.id

	FROM seminar_participant SP

    INNER JOIN seminar S

    	ON SP.id_seminar = S.id

WHERE id_user = 7445 -->

    <!-- Main content -->
    <section class="content" style="font-size:14px">
        <div class="container">

        <div class="">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                    <table id="dataTables" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name Event</th>
                                <th>Quota</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $show_seminar = mysqli_query($connect,"SELECT * FROM seminar");
                            while($row = mysqli_fetch_array($show_seminar)) {
                                $id_seminar = $row['id'];
                                $kuota_seminar = $row['quota'];
                        ?>
                        <tr>
                            <td>
                                <?php 
                                    echo "<a href='seminar-detail.php?id_seminar=".$row['id']." ' class='btn btn-success  btn-xs' '> ".$row['name']." </a>";
                                    echo "<br/><i style='font-size: 13px'> Date Event: ".$row['date_event']."</i>";
                                    echo "<br/><i style='font-size: 13px'> Posted on: ".$row['date_post']."</i>";
                                ?>
                            </td>
                            <td>
                                <?php 
                                    // Memunculkan jumlah partisipasi
                                    $show_seminar_partisipasi = mysqli_query($connect,"SELECT count(id) as partisipasi_membayar FROM `seminar_participant` where payment_status = 2 AND id_seminar = $id_seminar ");
                                    while($row2 = mysqli_fetch_array($show_seminar_partisipasi)) {
                                        $seminar_partisipasi = $row2['partisipasi_membayar'];
                                        echo " ".$seminar_partisipasi."/<b>".$row['quota']."</b>";

                                        // Cek untuk full atau tidak
                                        if($seminar_partisipasi==$kuota_seminar){
                                            echo " - <i style='color: red'> Full </i>";
                                            echo "<br/><i style='font-size: 13px'>".$seminar_partisipasi." Peserta sudah melakukan pembayaran</i>";
                                        } else {
                                            echo " ";
                                            echo "<br/><i style='font-size: 13px'>".$seminar_partisipasi." Peserta sudah melakukan pembayaran</i>";
                                        }
                                    }
                                ?>
                            </td>
                            <td>
                                <?php 
                                    if($row['pay_status'] == 1){
                                        echo "Paid - Rp", number_format($row['value']);
                                    } else {
                                        echo "Free";
                                    }
                                ?>
                            </td>
                        </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            <!-- /.box-body -->
            </div>
        <!-- /.box -->
            </div>
        </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php include("component/footer.php")?>