<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
    <div class="content-header">
        <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">
                    <a href="seminar-add.php" class='btn btn-primary'><i class="fas fa-plus"></i> Add Seminar</a>
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="seminar.php">Seminar</a></li>
                    <li class="breadcrumb-item active">Seminar</li>
                </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content" style="font-size:14px">
        <div class="container">
          <div class="box">
              <div class="box-body">

              </div>
          </div>
        </div>
    </section>

    </div>
<?php include("component/footer.php")?>