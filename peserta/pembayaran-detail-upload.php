x
<?php include("component/header.php")?>
    <?php include("component/navbar.php")?>
        <?php include("component/sidebar.php")?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">
                                    <a href="pembayaran.php" class='btn btn-primary'><i class="fas fa-arrow-left"></i> Back</a> Data Seminar - Payment
                                </h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="pambayaran.php">Payment</a></li>
                                    <li class="breadcrumb-item active">Payment</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content" style="font-size:14px">
                    <div class="container">

                        <div class="col-12 col-sm-12 col-lg-12">
                            <div class="card card-primary card-outline ">
                                <div class="card-header p-0 border-bottom-0">
                                    <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="custom-tabs-three-messages-tab" data-toggle="pill" href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages" aria-selected="false">Upload bukti pembayaran</a>
                                        </li>


                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content" id="custom-tabs-three-tabContent">

                                        <div class="tab-pane fade" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-lg-12" style="text-align: right">
                                                    <?php 
                                                      $id_seminar = $_GET['id_seminar'];
                                                      // echo "<a href='seminar-register.php?id_seminar=".$id_seminar."' class='btn btn-md btn-primary' style='margin-bottom: 20px;'><i class='fas fa-plus'></i> Daftar Sekarang </a>";
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Tab Document -->
                                        <div class="tab-pane fade active show" id="custom-tabs-three-messages" role="tabpanel" aria-labelledby="custom-tabs-three-messages-tab">
                                          <div class="row">
                                            <form role="form" method="POST" action="pembayaran-detail-upload.php" enctype="multipart/form-data">
                                              <div class="box-body">
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1"> Upload Foto : </label> <i><b style="color:red">*</b>jpg/png</i>
                                                  <input type="file" name="foto" required>
                                                </div>

                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Jumlah yang ditransfer</label>
                                                  <input type="number" class="form-control" id="payment_value" name="payment_value" required>
                                                </div>

                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Atas nama</label>
                                                  <input type="text" class="form-control" id="on_behalf" name="on_behalf" required>
                                                </div>
                                                
                                                <?php
                                                $id_seminar_participant = $_GET['id_seminar_participant'];
                                                echo "<input type='hidden' value='".$id_seminar_participant."' name='id_seminar_participant'>";
                                              
                                                ?>
                                                
                                              </div>
                                              <div class="box-footer">
                                                <a href="pembayaran.php" class="btn btn-primary">Kembali</a> 
                                                <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                                              </div>

                                                <?php
                                                  if(isset($_POST["submit"])){
                                                    $check = $_FILES["foto"]["tmp_name"];
                                                    if($check !== false){
                                                        $id_foto         = rand(100,99999);
                                                        $lokasi_file      = $_FILES['foto']['tmp_name'];
                                                        $nama_file        = $_FILES['foto']['name'];
                                                        $folder          = "../files-gambar/$id_foto+$nama_file";
                                                        $create_at       = (new DateTime('now'))->format('Y-m-d H:i:s');
                                                        $id_seminar_participant = $_POST['id_seminar_participant'];
                                                        $payment_value = $_POST['payment_value'];
                                                        $on_behalf =$_POST['on_behalf'];
                                                        
                                                        if (move_uploaded_file($lokasi_file,"$folder")){
                                                          echo "Nama File : <b>$nama_file</b> sukses di upload";
                                                          
                                                          $sql10 = "UPDATE seminar_participant SET foto='$id_foto+$nama_file',
                                                                  payment_value='$payment_value',
                                                                  on_behalf='$on_behalf',
                                                                  create_at='$create_at',
                                                                  payment_status=1
                                                                  WHERE id = '$id_seminar_participant'";

                                                          if ($connect-> query($sql10) === TRUE) {
                                                              echo "
                                                              <script type= 'text/javascript'>
                                                                  window.location = 'pembayaran.php';
                                                              </script>";
                                                              } else {
                                                                  echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                                                }
                                                          }else{
                                                              echo "<script type= 'text/javascript'>alert('File upload failed, please try again');</script>";
                                                          }
                                                        }
                                                        else{
                                                          echo "File gagal di upload";
                                                        }
                                                    }else{
                                                        echo "Please select an jpg/png file to upload.";
                                                    }
                                                ?>

                                            </form>
                                          </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>

                    </div>
                    <!--/. container-fluid -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <script>
                function RefreshFunction() {
                    location.reload();
                }
            </script>
            <?php include("component/footer.php")?>