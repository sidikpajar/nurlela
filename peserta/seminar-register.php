<?php include("component/header.php")?>
<?php include("component/navbar.php")?>
<?php include("component/sidebar.php")?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color: white; margin-top: 100px; margin-bottom: 100px">

      <!-- Main content -->
      <section class="content" style="font-size:14px">
          <div class="container">


          <?php
          if($_GET['id_seminar']) {
            $id_seminar        = $_GET['id_seminar'];
            $id_user           = $_SESSION['userid'];
            $id                = rand(1111,9999);
            $payment_status    = '';
            $quota             = '';
            $result_quota      = '';


            $cek_berbayar_atau_tidak ="SELECT seminar.pay_status as PAYSTATUS, seminar.quota as QUOTASEMINAR
              FROM seminar 
              WHERE id ='$id_seminar'";
            $query1 = mysqli_query( $connect, $cek_berbayar_atau_tidak );
            while($row = mysqli_fetch_array( $query1 )) {
              $quota = $row['QUOTASEMINAR'];
              if($row['PAYSTATUS'] == 0){
                $payment_status     = 3;
              } elseif($row['PAYSTATUS'] == 1){
                $payment_status     = 0;
              }
            }

            // Memunculkan jumlah partisipasi
            $show_seminar_partisipasi = mysqli_query($connect,"SELECT count(id) as partisipasi_membayar FROM `seminar_participant` where payment_status = 2 AND id_seminar = $id_seminar ");
            while($eksekusi_seminar_partisipasi = mysqli_fetch_array($show_seminar_partisipasi)) {
                $seminar_partisipasi = $eksekusi_seminar_partisipasi['partisipasi_membayar'];
                echo " Jumlah Partisipasi: ".$seminar_partisipasi."/".$quota."";
                echo "<br/>";
                echo " Kembali ke halaman sebelumnya: ";
                echo "<a href='javascript:history.back()'>kembali</a>";
                // Cek untuk full atau tidak
                if($seminar_partisipasi==$quota){
                  $result_quota     = 'KUOTA-PENUH';
                } else {
                  $result_quota     = 'KUOTA-TERSEDIA';
                }
            }

            if ($result_quota == 'KUOTA-PENUH'){
              echo "
              <script type= 'text/javascript'> 
              Swal.fire({
                  type: 'error',
                  title: 'Oops...Gagal mendaftar',
                  text: 'Kuota sudah penuh. Silakan mengikuti acara lainnya',
              })
              </script>";
            } else {
              
              $sql = "INSERT INTO seminar_participant (id,  id_seminar, id_user, payment_status)
              VALUES ('$id','$id_seminar','$id_user','$payment_status')
              ";

              if ($connect-> query($sql) === TRUE ) {
                echo "
                <script type= 'text/javascript'>
                  Swal.fire({
                      type: 'success',
                      title: 'Sukses mendaftar',
                      showConfirmButton: false,
                      timer: 1500
                  })
                </script>";
                }
            }
            $connect->close();
            }
          ?>


          </div>
      </section>

</div>
    <script>
      function RefreshFunction() {
        location.reload();
      }
    </script>
<?php include("component/footer.php")?>