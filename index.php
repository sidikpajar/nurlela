<?php
session_start();
if($_SESSION){
    if($_SESSION['user_level']=="admin")
    {
        header("Location: admin/index.php");
    }
    if($_SESSION['user_level']=="peserta")
    {
        header("Location: peserta/index.php");
    }
}

include('login.php');
?>

<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8" />
    <meta name="author" content="Script Tutorials" />
    <title>Presensi MyIndoversary 2019</title>
    <link href="styles.css" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <!-- Main css -->
    <link rel="stylesheet" href="css/style.css">  
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

</head>
<body>
<div id='stars'></div>
<div id='stars2'></div>
<div id='stars3'></div>
<?php
  include("db_connect.php");
?>

<div class="main">
<!-- Sing in  Form -->
<section class="sign-in" style="margin-top: 50px;">
    <div class="container">
            <div class="signin-form">
                
                <img style="padding-bottom: 30px;" src="stei.png"/>
                <form style="margin-bottom: 0px" method="post" action="" class="register-form" id="login-form">
                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px; " type="text" name="user_name" id="user_name" placeholder="Input Your NIM" required/>
                    </div>

                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-card-giftcard material-icons-name"></i></label>
                        <input style="font-size:15px; " type="password" name="user_password" id="user_password" placeholder="Input Your Password" required/>
                    </div>

                    <div style="padding-top: 0px;" class="form-group form-button">
                        <button type="submit" name="submit" value="Login" style="font-size:15px;" id="signin" class="form-submit">Login</button>
                        
                    </div>
                    <div style="padding-bottom: 0px;"  class="form-group form-button">
                        <a href="register.php" style="text-decoration: none; font-size:15px;">Registration here</a>
                    <div>
                </form>
            </div>
    </div>
</section>

</div>

<!-- JS -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>